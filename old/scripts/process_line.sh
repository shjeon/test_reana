#!/bin/bash
# Usage: ./process_line.sh input.txt line_number output_file

INPUT_FILE=$1
LINE_NUMBER=$2
OUTPUT_FILE=$3

# Ensure the output directory exists
mkdir -p $(dirname "$OUTPUT_FILE")

# Extract the specific line
LINE=$(sed -n "${LINE_NUMBER}p" "${INPUT_FILE}")

# Process and save the output
echo "Processed: $LINE" > "$OUTPUT_FILE"

