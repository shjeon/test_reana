import os
import json
import subprocess
import sys
import shutil

def setup_and_run_contur(param_file, template_file, ufo_dir=None):
    # Create directory
    os.makedirs("ReanaTest", exist_ok=True)
    os.chdir("ReanaTest")
    
    # If UFO directory exists, copy it
    if ufo_dir and os.path.exists(ufo_dir):
        ufo_name = os.path.basename(ufo_dir.rstrip('/'))
        shutil.copytree(ufo_dir, ufo_name, dirs_exist_ok=True)
    
    # Run contur-batch with provided parameter and template files
    cmd = (
        f"contur-batch --beam 13TeV --mceg madgraph "
        f"--param-steering-file {param_file} "
        f"--template {template_file} "
        f"-n 500 --scan-only"
    )
    subprocess.run(cmd, shell=True, check=True)
    
    # Find directories under myscan00/13TeV/
    scan_dir = "myscan00/13TeV"
    if not os.path.exists(scan_dir):
        raise RuntimeError(f"Expected directory {scan_dir} not found")
        
    # Get only directories (skip files)
    run_dirs = [d for d in os.listdir(scan_dir) 
                if os.path.isdir(os.path.join(scan_dir, d))]
    
    # Save the directory information
    with open("run_directories.json", "w") as f:
        json.dump({
            "directories": run_dirs,
            "base_path": os.path.abspath(scan_dir)
        }, f)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python setup_contur.py <param_file> <template_file> [ufo_dir]")
        sys.exit(1)
    
    param_file = sys.argv[1]
    template_file = sys.argv[2]
    ufo_dir = sys.argv[3] if len(sys.argv) > 3 else None
    
    setup_and_run_contur(param_file, template_file, ufo_dir)
