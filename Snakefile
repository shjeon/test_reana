import json
import os

# Get parameter and template file names from config
PARAM_FILE = config["param_file"]
TEMPLATE_FILE = config["template_file"]
UFO_DIR = config.get("ufo_dir", "")  # Optional UFO directory

rule all:
    input:
        "completion_flag.txt"

rule setup_contur:
    input:
        param=PARAM_FILE,
        template=TEMPLATE_FILE
    output:
        "ReanaTest/run_directories.json"
    params:
        ufo_dir=UFO_DIR
    shell:
        """
        if [ -z "{params.ufo_dir}" ]; then
            python setup_contur.py {input.param} {input.template}
        else
            python setup_contur.py {input.param} {input.template} {params.ufo_dir}
        fi
        """

rule run_point:
    input:
        "ReanaTest/run_directories.json"
    output:
        "ReanaTest/myscan00/13TeV/{directory}/completed.flag"
    params:
        directory="{directory}"
    shell:
        """
        cd ReanaTest/myscan00/13TeV/{params.directory} && \
        bash runpoint_{params.directory}.sh && \
        touch completed.flag
        """

rule finalize:
    input:
        json="ReanaTest/run_directories.json"
    output:
        "completion_flag.txt"
    run:
        with open(input.json) as f:
            data = json.load(f)
            
        # Generate input files list based on directories
        input_flags = expand(
            "ReanaTest/myscan00/13TeV/{directory}/completed.flag",
            directory=data["directories"]
        )
        
        # Check all flags exist
        for flag in input_flags:
            if not os.path.exists(flag):
                raise RuntimeError(f"Missing completion flag: {flag}")
        
        # Create final flag
        with open(output[0], "w") as f:
            f.write("All jobs completed successfully\n")
